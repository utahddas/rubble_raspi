#!/usr/bin/python 
import rospy
import time
from threading import Timer
from std_msgs.msg import Int16, Float32, Bool, String

##########################
## Constants
##########################

Ts = 0.01           # update rate in seconds (100 Hz)
Ki = 0.0            # Integrator constant
#Ki = 0.317
Kg = 6.0              # Gain in PID control
Kd = 200.0
derivator = 0
integrator = 0
integrator_max = 1
integrator_min = 1


B1 = 0.8758         # Used with ax_in in High Pass Filter
B2 = -1.7517        # "
B3 = 0.8758         # "
A1 = 1              # Not used
A2 = -1.9647        # Used with ax_out in High Pass Filter
A3 = 0.9660         # "

##########################
## Variables
##########################

des_pos      = 0 # This is the commanded position
curr_pos     = 0 # This is the current position, obtained via callback
curr_vel     = 0 # This is the current velocity, obtained via callback
focus        = 0 # This tells the node whether or not to publish

killswitch = False
killmotors = False

QUEUE_SIZE = 100 # 
PUBLISH = True   # If true, the program will publish to rostopics
DEBUG = False    # If true, print statements will print to the console


##########################
## Callback funtions
##########################

'''Callback functions'''
def callback_curr_pos(msg):
    global curr_pos
    curr_pos = msg.data

def callback_curr_vel(msg):
    global curr_vel
    curr_vel = msg.data

def callback_des_pos(msg):
    global des_pos
    global derivator
    global integrator
    des_pos = msg.data
    derivator = 0
    integrator = 0

def callback_focus(msg):
    global focus
    focus = msg.data

def callback_killswitch(msg):
    global killswitch
    killswitch = msg.data

def callback_killmotors(msg):
    global killmotors
    killmotors = msg.data

##########################
## ROS Initialization
##########################

'''Create the node'''
rospy.init_node("lidar_controller")

'''Create the publisher/subscriber objects for the appropriate rostopics'''

# Publishers
left_motor_pub = rospy.Publisher("/rubble/motor/left/spd", Int16, queue_size = QUEUE_SIZE)
right_motor_pub = rospy.Publisher("/rubble/motor/right/spd", Int16, queue_size = QUEUE_SIZE)
curr_pos_pub = rospy.Publisher("/rubble/state/position/x", Float32, queue_size = QUEUE_SIZE)

# Subscriber
rospy.Subscriber('/rubble/lidar/pos', Float32, callback_curr_pos)
rospy.Subscriber('/rubble/lidar/vel', Float32, callback_curr_vel)
rospy.Subscriber('/rubble/command/pos', Float32, callback_des_pos)
rospy.Subscriber('/rubble/command/focus', String, callback_focus)
rospy.Subscriber('/rubble/error/killswitch', Bool, callback_killmotors)
rospy.Subscriber('/rubble/error/killmotors', Bool, callback_killmotors)

##########################
## Other Functions
##########################

def convert(new_control):
    MAX_DISTANCE = 2.0
    MAX_SPEED = 150

    ratio = new_control / MAX_DISTANCE
    if ratio > 1:
        ratio = 1
    elif ratio < -1:
        ratio = -1

    bitwise_spd = ratio*MAX_SPEED

    if DEBUG:
        print "new_control: " + str(new_control)
        print "ratio: " + str(ratio)
        print "bitwise_spd: " + str(bitwise_spd)
    return bitwise_spd

# This is the integrator to help the motor get enough speed to start
i_term = 0.0
old_diff = (des_pos - curr_pos)

##########################
## Main Loop
##########################
try:
    while not rospy.is_shutdown() and killswitch is False:

        ##########################
        ## Control
        ##########################

        # Compute the diff between des pos and curr pos, then scale with gain
        error = des_pos - curr_pos
        control = Kg*error

        '''
        new_diff = error
        if new_diff != old_diff or abs(error) < .25:
            i_term = 0
        else:
            i_term += 0.01

        old_diff = new_diff
        '''

        d_term = Kd * (error - derivator)
        derivator = error

        integrator = integrator + error

        if integrator > integrator_max:
            integrator = integrator_max
        elif integrator < integrator_min:
            integrator = integrator_min

        i_term = integrator * Ki


        # Convert from volts to bits and clamp to max/min
        control_input = convert(control + i_term + d_term)

        # Kill commands to the motors if prompted to.
        if killmotors is False and focus == 'dist':
            left_motor_pub.publish(control_input)
            right_motor_pub.publish(control_input)

        # Always ok to publish the current position though
        curr_pos_pub.publish(curr_pos)

        # Sleep for set time
        time.sleep(Ts)


except KeyboardInterrupt:
    pass

print "Control: " + str(control)
print "Control Input: " + str(control_input)
