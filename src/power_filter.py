#!/usr/bin/python
import rospy
import time

from numpy import median
from std_msgs.msg import UInt16, Int16, Float32

''' Constants '''
QUEUE_SIZE = 1
NUM_ENTRIES = 25
DEBUG = True

''' Variables '''
lreadings = []
lreadings_size = NUM_ENTRIES
rreadings = []
rreadings_size = NUM_ENTRIES
vreadings = []
vreadings_size = NUM_ENTRIES

''' Callback functions '''
def callback_lcur(msg):
    global lreadings
    add_reading(lreadings, msg.data)

def callback_rcur(msg):
    global rreadings
    add_reading(rreadings, msg.data)

def callback_volt(msg):
    global vreadings
    add_reading(vreadings, msg.data)

# Returns a list filled with 0's (initialized)
def init_readings(num_readings):
    new_readings_list = []
    for i in range(0,num_readings):
        new_readings_list.append(0)
    return new_readings_list

# Returns the median of the current readings list (acts as a filter)
def filter_readings(readings):
    return median(readings)

def add_reading(readings_list, new_reading):
    readings_list.pop(0)
    readings_list.append(new_reading)

lreadings = init_readings(lreadings_size)
rreadings = init_readings(rreadings_size)
vreadings = init_readings(vreadings_size)

''' Initialize ROS variables '''
rospy.init_node("power_filter")
pub_lcur = rospy.Publisher("/rubble/state/power/current/left", Int16, queue_size = QUEUE_SIZE)
pub_rcur = rospy.Publisher("/rubble/state/power/current/right", Int16, queue_size = QUEUE_SIZE)
pub_cursum = rospy.Publisher("/rubble/state/power/current/input", Int16, queue_size = QUEUE_SIZE)
pub_voltage = rospy.Publisher("/rubble/state/power/voltage", Float32, queue_size = QUEUE_SIZE)
rospy.Subscriber("/rubble/trex/current/left", Int16, callback_lcur)
rospy.Subscriber("/rubble/trex/current/right", Int16, callback_rcur)
rospy.Subscriber("/rubble/trex/voltage", Float32, callback_volt)


''' Main loop '''
while not rospy.is_shutdown():

    # Obtain current readings
    lmedian = filter_readings(lreadings)
    rmedian = filter_readings(rreadings)
    sumcur = lmedian + rmedian

    # Get voltage reading
    volt = filter_readings(vreadings)

    # Publish the power topics
    pub_lcur.publish(lmedian)
    pub_rcur.publish(rmedian)
    pub_cursum.publish(sumcur)
    pub_voltage.publish(volt)
