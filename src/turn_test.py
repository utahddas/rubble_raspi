#!/usr/bin/python
import rospy
import time
from std_msgs.msg import Int16

##########################
## Constants
##########################

Ts = 0.01           # update rate in seconds (100 Hz)

##########################
## Variables
##########################

direction = 0
QUEUE_SIZE = 100

##########################
## Callback funtions
##########################

def callback_turn_test(msg):
    global direction
    direction = msg.data

##########################
## ROS Initialization
##########################

''' Create the node '''
rospy.init_node("turn_test")

''' Create the publisher/subscriber objects for the appropriate rostopics '''
# Publishers
left_motor_pub = rospy.Publisher("/rubble/motor/left/spd", Int16, queue_size = QUEUE_SIZE)
right_motor_pub = rospy.Publisher("/rubble/motor/right/spd", Int16, queue_size = QUEUE_SIZE)

# Subscribers
rospy.Subscriber('/rubble/test/turn', Int16, callback_turn_test)


##########################
## Main Loop
##########################
while not rospy.is_shutdown() :
    #SPEED = 220
    SPEED = 255

    # If the value is less than 0, then it should rotate counterclockwise
    if direction < 0:
        #left_motor_pub.publish(-0.10*SPEED)
        left_motor_pub.publish(-0.15*SPEED)
        right_motor_pub.publish(SPEED)

    # If the value is greater than 0, then it should rotate clockwise
    elif direction > 0:
        left_motor_pub.publish(SPEED)
        #right_motor_pub.publish(-0.10*SPEED)
        right_motor_pub.publish(-0.15*SPEED)
    else:
        left_motor_pub.publish(0)
        right_motor_pub.publish(0)

    # Sleep for set time
    time.sleep(Ts
