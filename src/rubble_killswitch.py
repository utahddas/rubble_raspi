#!/usr/bin/python
import rospy

from std_msgs.msg import Bool, Int16, Float32

# Constants
QUEUE_SIZE = 10

lidarobs = False

def callback_error_lidarobs(msg):
    global lidarobs
    lidarobs = msg.data



''' Create the node '''
rospy.init_node("error_controller")

# Publishers
# Kill Switches
killswitch_pub = rospy.Publisher("/rubble/error/killswitch", Bool, queue_size = QUEUE_SIZE) # Master Killswitch
killmotor_pub = rospy.Publisher("/rubble/error/killmotors", Bool, queue_size = QUEUE_SIZE)  # Kills motors explicitly

motor_left_pub = rospy.Publisher("/rubble/motor/left/spd", Int16, queue_size = QUEUE_SIZE)
motor_right_pub = rospy.Publisher("/rubble/motor/right/spd", Int16, queue_size = QUEUE_SIZE)
pos_calib_pub = rospy.Publisher("/rubble/state/calibrate", Bool, queue_size = QUEUE_SIZE)
pos_command_pub = rospy.Publisher("/rubble/command/pos", Float32, queue_size = QUEUE_SIZE)


# Subscribers
rospy.Subscriber('/rubble/error/lidarobs', Bool, callback_error_lidarobs)

while not rospy.is_shutdown():
    if lidarobs is True:

        # Flip the switch
        killmotor_pub.publish(True)

        # Kill the motors
        motor_left_pub.publish(0)
        motor_right_pub.publish(0)

        # Calibrate and set command to 0
        pos_calib_pub.publish(True)
        pos_command_pub.publish(0)

