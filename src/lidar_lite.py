import smbus
import time

class Lidar_Lite():
  def __init__(self):
    self.address = 0x62
    self.distWriteReg = 0x00
    self.distWriteValNoBias = 0x03
    self.distWriteVal = 0x04
    self.distReadReg1 = 0x8f
    self.distReadReg2 = 0x10
    self.velWriteReg = 0x04
    self.velWriteVal = 0x08
    self.velReadReg = 0x09
    self.outerLoopCountDef = 0x00
    self.outerLoopCountFree = 0xff
    self.outerLoopCountReg = 0x11
    self.measureDelayReg = 0x45
    self.acquireCommandReg = 0x00
    self.acquireConfigReg = 0x04
    self.lastReading = 0
    self.currReading = 0
    self.rate = 2000

  def connect(self, bus):
    try:
      self.bus = smbus.SMBus(bus)
      time.sleep(0.5)
      return 0
    except:
      return -1

  def setFreeMode(self):
      '''
      Set the sampling frequency as 2000/Hz
      10 Hz = 0xc8
      20 Hz = 0x64
      100 Hz = 0x14
      '''
      self.writeAndWait(self.outerLoopCountReg, self.outerLoopCountFree) # Set to continuous sampling after initial read
      #self.writeAndWait(self.measureDelayReg, int(2000 / self.rate)) # Set the sampling frequency
      self.writeAndWait(self.acquireCommandReg, 0x04) # Include receiver bias correction 0x04

      '''
      Acquisition config register
      0x01 Data ready interrupt
      0x20 Take sampling rate from MEASURE_DELAY
      '''
      #self.writeAndWait(self.acquireConfigReg, 0x21)


  def setDefMode(self):
      self.writeAndWait(self.outerLoopCountReg, self.outerLoopCountDef)

  def writeAndWait(self, register, value):
    self.bus.write_byte_data(self.address, register, value);
    time.sleep(0.02)

  def readAndWait(self, register):
    res = self.bus.read_byte_data(self.address, register)
    time.sleep(0.02)
    return res

  def readDistAndWait(self, register):
    res = self.bus.read_i2c_block_data(self.address, register, 2)
    time.sleep(0.02)
    return (res[0] << 8 | res[1])

  def getDistance(self):
    self.writeAndWait(self.distWriteReg, self.distWriteVal)
    dist = self.readDistAndWait(self.distReadReg1)
    #print "Distance: " + str(dist)
    return dist

  def getVelocity(self):
    self.writeAndWait(self.distWriteReg, self.distWriteVal)
    self.writeAndWait(self.velWriteReg, 0x80)
    vel = self.readAndWait(self.velReadReg)
    #print "Velocity: " + str(vel)
    return self.signedInt(vel)
    

  def getDistanceFree(self):
      dist = self.readDistAndWait(self.distReadReg1)
      return dist

  def getVelocityFree(self):
      vel = self.readAndWait(self.velReadReg)
      return self.signedInt(vel)

  def signedInt(self, value):
    if value > 127:
      return (256-value) * (-1)
    else:
      return value

  def read(self):
    self.lastReading = self.currReading
    self.currReading = self.getDistance()
    velocity = (self.currReading - self.lastReading) 
    return self.currReading, velocity
