#!/usr/bin/python

import rospy
import time
import numpy as np
from threading import Timer
from std_msgs.msg import Float32

##########################
## Constants
##########################

Ts = 0.01           # update rate in seconds (100 Hz)
QUEUE_SIZE = 100    
DEBUG = False       # If true, print statements will print to the console

##########################
## Variables
##########################

comx = 0
comy = 0
curx = 0
cury = 0

##########################
## Callback funtions
##########################

'''Callback functions'''
def callback_comx(msg):
    global comx
    comx = msg.data

def callback_comy(msg):
    global comy
    comy = msg.data

def callback_curx(msg):
    global curx
    curx = msg.data

def callback_cury(msg):
    global cury
    cury = msg.data


##########################
## ROS Initialization
##########################

'''Create the node'''
rospy.init_node("coordinate_translator")

'''Create the publisher/subscriber objects for the appropriate rostopics'''

# Publishers
command_dist_pub = rospy.Publisher("/rubble/command/dist", Float32, queue_size = QUEUE_SIZE)
command_ang_pub = rospy.Publisher("/rubble/command/ang", Float32, queue_size = QUEUE_SIZE)

# Subscriber
rospy.Subscriber('/rubble/command/x', Float32, callback_comx)
rospy.Subscriber('/rubble/command/y', Float32, callback_comy)
rospy.Subscriber('/rubble/state/position/x', Float32, callback_curx)
rospy.Subscriber('/rubble/state/position/y', Float32, callback_cury)

##########################
## Main Loop
##########################
try:
    while not rospy.is_shutdown():

        offx = comx - curx
        offy = comy - cury

        dist = np.sqrt(offx**2 + offy**2)
        theta_rad = np.arccos(offx/dist)
        theta_deg = theta_rad * (180/np.pi)

        print dist
        print theta_deg

        command_dist_pub.publish(dist)
        command_ang_pub.publish(theta_deg)

        # Sleep for set time
        time.sleep(Ts)


except KeyboardInterrupt:
    pass
