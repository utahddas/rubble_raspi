#!/usr/bin/python

import rospy
import time

from numpy import median
from std_msgs.msg import Int32, Float32, Bool, String
from lidar_lite import Lidar_Lite 

''' Constants '''
QUEUE_SIZE = 1024
NUM_ENTRIES = 10
DEBUG = True

''' Variables '''
# Distance and velocity variables
distances = []
distances_size = 8 
current_dist = 0
past_dist = 0
focus = ""

# Calibration variables
calibration = False
calibration_size = 100
zero_dist = 0

# Killswitch variables
killswitch = False

''' Useful Functions '''
# Designed to calibrate the distance we will call zero
def calibrate(lidar, calibration_size):
    calibration_list = []
    for i in range (0, calibration_size):
        dist, vel = lidar.read()
        calibration_list.append(dist)
    mean_dist = float(sum(calibration_list))/len(calibration_list)
    return mean_dist

# Returns a list filled with distances (initialized)
def init_distances(lidar, distances_size):
    distances = []
    for i in range (0, distances_size):
        dist, vel = lidar.read()
        distances.append(dist)
    return distances

# Finds current pos using the diff between its current pos and the zero pos
# Useful for cleaning up code, and for modifying each instance quickly
def compute_pos(zero_pos, dist):
    return zero_pos - dist

# Returns the mean of the distances list (acting as a filters)
def filter_dist(distances):
    return median(distances)
    #return float(sum(distances))/len(distances)

def add_dist(distances, dist):
    distances.pop(0)
    distances.append(dist)

def callback_focus(msg):
    global focus
    global calibration
    focus = msg.data
    if focus == "dist":
        calibration = True
    
''' Callback functions '''
def callback_calibrate(msg):
    global calibration
    calibration = msg.data

def callback_killswitch(msg):
    global killswitch
    killswitch = msg.data

''' Initialize ROS variables '''
rospy.init_node("translation")
pub_dist = rospy.Publisher("/rubble/lidar/dist", Float32, queue_size = QUEUE_SIZE)
pub_pos = rospy.Publisher("/rubble/lidar/pos", Float32, queue_size = QUEUE_SIZE)
pub_vel = rospy.Publisher("/rubble/lidar/vel", Float32, queue_size = QUEUE_SIZE)
pub_lidarobs = rospy.Publisher("/rubble/error/lidarobs", Bool, queue_size = QUEUE_SIZE) 
rospy.Subscriber('/rubble/state/calibrate', Bool, callback_calibrate)
rospy.Subscriber('/rubble/error/killswitch', Bool, callback_killswitch)
rospy.Subscriber('/rubble/command/focus', String, callback_focus)

''' Create LIDAR object and calibrate '''
lidar = Lidar_Lite()
connected = lidar.connect(1)
lidar.setFreeMode()

if connected <= -1:
    print "Not Connected..."

# Calibrate before entering the main loop
zero_pos = calibrate(lidar, calibration_size)
distances = init_distances(lidar, distances_size)
past_dist = filter_dist(distances)
past_pos = compute_pos(zero_pos, past_dist)
start = time.time()

''' Main loop '''
while not rospy.is_shutdown():

    # First, check to see if we need to calibrate again
    if calibration is True:
        zero_pos = calibrate(lidar, calibration_size)
        distances = init_distances(lidar, distances_size)
        past_dist = filter_dist(distances)
        past_pos = compute_pos(zero_pos, past_dist)
        start = time.time()
        calibration = False

    # Get the dist/vel (we only use dist though)
    dist, vel = lidar.read()
    end = time.time()

    # Filter the reading and compute new position and velocity
    add_dist(distances, dist)
    curr_dist = filter_dist(distances)
    curr_pos = compute_pos(zero_pos, curr_dist)
    if curr_dist < 50.0 or abs(curr_pos - past_pos) > 100.0:
        pub_lidarobs.publish(True)

    vel = float(curr_pos - past_pos)/(100*(end-start))

    # Shift our time placeholders
    start = end
    past_dist = curr_dist
    past_pos = curr_pos

    # Publish results in ROS (be sure to convert from cm to m)
    pub_dist.publish(curr_dist/100)
    pub_pos.publish(curr_pos/100)
    pub_vel.publish(vel/100)   
