#!/usr/bin/python

import rospy
import time
from threading import Timer
from std_msgs.msg import Int16, Float32, Bool, String

##########################
## Constants
##########################

Ts = 0.01           # update rate in seconds (100 Hz)
Ki = 0.0            # Integrator constant
#Ki = 0.317
Kg = 6.0              # Gain in PID control
Kd = 200.0
derivator = 0
integrator = 0
integrator_max = 1
integrator_min = 1

B1 = 0.8758         # Used with ax_in in High Pass Filter
B2 = -1.7517        # "
B3 = 0.8758         # "
A1 = 1              # Not used
A2 = -1.9647        # Used with ax_out in High Pass Filter
A3 = 0.9660         # "

##########################
## Variables
##########################

des_ang      = 0 # This is the commanded position
curr_ang     = 0 # This is the current position, obtained via callback
dof_status   = 0 # This is the current velocity, obtained via callback
focus        = ''# This tells the node when to measure the angle
direction    = ''# This tells the car what direction to turn in

killswitch = False
killmotors = False

QUEUE_SIZE = 100 # 
PUBLISH = True   # If true, the program will publish to rostopics
DEBUG = False    # If true, print statements will print to the console


##########################
## Callback funtions
##########################

'''Callback functions'''
def callback_curr_angle(msg):
    global curr_ang
    curr_ang = msg.data

def callback_des_angle(msg):
    global des_ang
    global derivator
    global integrator
    des_ang = msg.data
    derivator = 0
    integrator = 0

def callback_dof_status(msg):
    global dof_status
    dof_status = msg.data

def callback_focus(msg):
    global focus
    focus = msg.data

def callback_direction(msg):
    global direction
    direction = msg.data

def callback_killswitch(msg):
    global killswitch
    killswitch = msg.data

def callback_killmotors(msg):
    global killmotors
    killmotors = msg.data

##########################
## ROS Initialization
##########################

'''Create the node'''
rospy.init_node("lidar_controller")

'''Create the publisher/subscriber objects for the appropriate rostopics'''

# Publishers
left_motor_pub = rospy.Publisher("/rubble/motor/left/spd", Int16, queue_size = QUEUE_SIZE)
right_motor_pub = rospy.Publisher("/rubble/motor/right/spd", Int16, queue_size = QUEUE_SIZE)
curr_ang_pub = rospy.Publisher("/rubble/state/angle/x", Float32, queue_size = QUEUE_SIZE)

# Subscriber
rospy.Subscriber('/rubble/dof/axis/x', Float32, callback_curr_angle)
rospy.Subscriber('/rubble/dof/status', Bool, callback_dof_status)
rospy.Subscriber('/rubble/command/ang', Float32, callback_des_angle)
rospy.Subscriber('/rubble/command/focus', String, callback_focus)
rospy.Subscriber('/rubble/command/direction', String, callback_direction)
rospy.Subscriber('/rubble/error/killswitch', Bool, callback_killswitch)
rospy.Subscriber('/rubble/error/killmotors', Bool, callback_killmotors)

##########################
## Other Functions
##########################

def efficient_angle(angle1, angle2):
    diff = (angle2 - angle1 + 180) % 360 - 180
    if diff < -180:
        diff = diff + 360
    return diff

def convert(new_control):
    SPEED = 255

    # Turn right forward
    if new_control < 0 and direction == 'forward':
        #return (SPEED, -(SPEED))
        return (SPEED, -0.15*SPEED)

    # Turn left forward
    elif new_control > 0 and direction == 'forward':
        #return (-(SPEED), SPEED)
        return(-0.15*SPEED, SPEED)

    # Turn right backward
    elif new_control < 0 and direction == 'backward':
        return(0.15*SPEED, -SPEED)

    elif new_control > 0 and direction == 'backward':
        return(-SPEED, 0.15*SPEED)

    # Turn left backward

    # Stay still
    else:
        return (0,0)

# This is the integrator to help the motor get enough speed to start
i_term = 0.0
old_diff = efficient_angle(des_ang, curr_ang)

##########################
## Main Loop
##########################
try:
    while not rospy.is_shutdown() and killswitch is False:

        ##########################
        ## Control
        ##########################

        # Compute the diff between dangpos and curr pos, then scale with gain
        error = efficient_angle(des_ang, curr_ang)
        control = Kg*error

        '''
        new_diff = error
        if new_diff != old_diff or abs(error) < .25:
            i_term = 0
        else:
            i_term += 0.01

        old_diff = new_diff
        '''

        d_term = Kd * (error - derivator)
        derivator = error

        integrator = integrator + error

        if integrator > integrator_max:
            integrator = integrator_max
        elif integrator < integrator_min:
            integrator = integrator_min

        i_term = integrator * Ki


        # Convert from volts to bits and clamp to max/min 
        control_lw, control_rw = convert(control + i_term + d_term)

        # Kill commands to the motors if prompted to.        
        if killmotors is False and abs(error) > 5 and focus == "angle":
            left_motor_pub.publish(control_lw)
            right_motor_pub.publish(control_rw)

        # If we are within our error, or the motors have been killed
        # Command the motors to go to 0
        elif focus == 'angle':
            left_motor_pub.publish(0)
            right_motor_pub.publish(0)

        # Always ok to publish the current position though
        curr_ang_pub.publish(curr_ang)

        # Sleep for set time
        time.sleep(Ts)


except KeyboardInterrupt:
    pass

print "Control: " + str(control)
print "Control Input: " + str(control_rw)                                          
